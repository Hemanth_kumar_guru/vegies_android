package com.hemanthdev.vegies.di.scope

import javax.inject.Scope

/**
 *<h1></h1>

 *<p></p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FragmentScoped