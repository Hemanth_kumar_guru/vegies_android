package com.hemanthdev.vegies.di


import dagger.Module

@Module(
    includes = [ViewModelModule::class]
)
abstract class ActivityBindingModule {


}