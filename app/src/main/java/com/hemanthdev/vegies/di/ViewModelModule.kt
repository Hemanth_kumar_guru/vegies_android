package com.hemanthdev.vegies.di

import dagger.Module

/**
 *<h1></h1>

 *<p>
 * The ViewModelModule is used to provide a mapChatData of view models through dagger that is used by the ViewModelFactoryModule class.
 *</p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */

@Module
abstract class ViewModelModule : ViewModelFactoryModule() {

    /*
    * This method basically says
    * inject this object into a Map using the @IntoMap annotation,
    * with the  LoginViewModel.class as key,
    * and a Provider that will build a LoginViewModel
    * object.
    *
    * */
}