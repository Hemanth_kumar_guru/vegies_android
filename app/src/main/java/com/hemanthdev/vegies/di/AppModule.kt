package com.hemanthdev.vegies.di

import android.content.Context
import com.hemanthdev.AppController
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

/**
 *<h1></h1>

 *<p></p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */
@Module
abstract class AppModule {

    @Singleton
    @Binds
    abstract fun bindApplication(appController: AppController): Context
}