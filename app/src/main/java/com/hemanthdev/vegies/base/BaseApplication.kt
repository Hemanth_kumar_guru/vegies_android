package com.hemanthdev.vegies.base

import android.content.Context
import android.os.StrictMode
import dagger.android.support.DaggerApplication
/**
 *<h1></h1>

 *<p></p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */
abstract class BaseApplication : DaggerApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
//        MultiDex.install(this)
    }

}