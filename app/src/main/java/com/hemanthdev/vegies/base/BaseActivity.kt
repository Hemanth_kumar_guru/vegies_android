package com.hemanthdev.vegies.base

import dagger.android.support.DaggerAppCompatActivity

/**
 *<h1></h1>

 *<p></p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */
class BaseActivity : DaggerAppCompatActivity() {
}