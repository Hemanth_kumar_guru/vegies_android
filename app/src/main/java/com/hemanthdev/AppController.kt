package com.hemanthdev

import com.hemanthdev.vegies.base.BaseApplication
import com.hemanthdev.vegies.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

/**
 *<h1></h1>

 *<p></p>

 * @author : Hemanth
 * @since : 22 Jul 2020
 * @version : 1.0
 * @company : 3Embed Software Technologies Pvt. Ltd.
 */
class AppController : BaseApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
        appComponent.inject(this)
        return appComponent
    }
}